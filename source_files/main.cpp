﻿#include <iostream>
#include <vector>
#include <string>
#include <filesystem>
#include <fstream>
#include "file_directory.h"
#include <stdexcept>
#include <conio.h> 

using std::vector;
using std::string;
using std::cout;
using std::endl;

namespace fs = std::filesystem;

void mainMenu(int mainLoopIterrator)
{
    string userPath;
    cout << "Please write path to directory you want to check (use \"/\"): " << endl;
    std::getline(std::cin, userPath);
    if (!(checkPath(userPath) == 2))
    {
        while ((!(checkPath(userPath) == 2)))
        {
            cout << "You entered wrong path. Please try again:" << endl;
            std::getline(std::cin, userPath);
        }
    }

    Directory dir1(userPath);                                   //it is checked directory path so no need to use try/ catch
    vector<File> dir1Files = dir1.directoryGetFiles();
    vector<fs::path> dir1Sub = dir1.directoryGetSubdirectories();
    cout << "\nThis directory contains " << dir1Files.size() << " files. " << endl;
    cout << "And " << dir1Sub.size() << " subdirectories. " << endl;
    cout << "If you want to print their names please press [SPACE BAR], if you want to continue without it press any button. " << endl;

    char keyboardSign = _getch();
    if (keyboardSign == ' ')
    {
        cout << "\nFiles:" << endl;
        dir1.directoryPrintFiles();
        cout << "Subdirectories: " << endl;
        dir1.directoryPrintSubdirectories();
    }

    cout << "\nWhat kind of files do you want to examine. Please write extensions in one line with space between them.";
    cout << endl << "For example: \".h .cpp\"" << endl;

    string extensionsFromUser;
    //std::cin.ignore();
    std::getline(std::cin, extensionsFromUser);
    dir1.directoryFiltrExtensions(extensionsFromUser);

    string reportFileName = "report" + std::to_string(mainLoopIterrator);
    dir1.directoryGenerateReport(reportFileName);

    cout << "Program generated report file " << reportFileName + ".txt in this directory:"
        << fs::current_path();

    string systemName = "notepad " + fs::current_path().string() + "\\" + reportFileName;
    system(systemName.c_str());
}

int main()
{
    int mainLoopIterator{ 1 };
    while (1)
    {
        mainMenu(mainLoopIterator);

        cout << "\n \nIf you want to generate another report please press [SPACE BAR]." << endl;
        cout << "If you want to finish this program press another button." << endl;

        char keyboardSign = _getch();
        if (keyboardSign != ' ')
            break; 
    }
}
