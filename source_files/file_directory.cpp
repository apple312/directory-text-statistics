#include <iostream>
#include <vector>
#include <string>
#include <filesystem>
#include <fstream>
#include "file_directory.h"
#include <stdexcept>
#include <sstream>

using std::vector;
using std::string;
using std::cout;
using std::endl;

namespace fs = std::filesystem;

/********************************************************
*********************   File CLASS   ********************
*********************************************************/
File::File(fs::path userPath)
{
    //check if user entered correct file path
    if (!(fs::is_regular_file(userPath)))
        throw std::runtime_error("given path is not regular file path");

    filePath = userPath;

    fileExtension = filePath.extension().string();
    fileSize = fs::file_size(filePath);

    std::ifstream file(filePath);
    string line;
    while (std::getline(file, line))
    {
        fileLines++;

        if (line.size() == 0)
            continue;
        fileChars += line.size();
        
        //check number of words, by using i'th and i-1'th char of string type line, so 1st char must be checked alone 
        for (int i = 0; i <= line.size() - 1; i++)
        {
            if (i == 0)
            {
                if (line[i] != ' ')
                    fileWords++;
            }
            else if (line[i] != ' ' && line[i - 1] == ' ')
                fileWords++;
        }
    }
    file.close();
}

fs::path File::fileGetPath(void)
{
    return filePath;
}

string File::fileGetExt(void)
{
    return fileExtension.c_str();
}

// overolad of << operator 
std::ostream& operator<<(std::ostream& os, File& f)
{
    os << "File path:        " << f.fileGetPath().string() << endl;
    os << "File size:        " << f.fileSize << "bytes" << endl;
    os << "Number of lines:  " << f.fileLines << endl;
    os << "Number of words:  " << f.fileWords << endl;
    os << "Number of chars:  " << f.fileChars << endl;

    return os;
}

/********************************************************
*****************   Directory CLASS   *******************
*********************************************************/
Directory::Directory(fs::path userPath)
{
    //check if user entered directory path
    if (!(fs::is_directory(userPath)))
        throw std::runtime_error("given path is not directory path");

    directoryPath = userPath;

    fs::path starting_path(userPath);											//change string path from user to fs::path
    vector<fs::path> paths_to_check{ starting_path };							//create vector for path of directories that program should search for files 
    int i{ 0 };
    while (1)
    {
        for (const auto& entry : fs::directory_iterator(paths_to_check[i]))
        {
            if (fs::is_directory(entry))
            {
                directorySubdirectoriesPaths.push_back(entry.path());
                paths_to_check.push_back(entry.path());							//add path of directory to check					
            }
            else if (fs::is_regular_file(entry))
            {
                File file(entry.path());
                directoryFiles.push_back(file);                                 //add file path to database
            }
            else
                continue;	                               
        }

        //if there is no more directories, function should end 
        //otherwise should do another loop with bigger iterator
        if (paths_to_check.size() == i + 1)
            break;
        else
            i++;
    }
}

void Directory::directoryPrintFiles(void)
{
    int i{ 0 };
    for (auto n : directoryFiles)
    {
        i++;
        cout << i <<". " << n.fileGetPath().filename() << endl;
    }
}

void Directory::directoryPrintSubdirectories(void)
{
    int i{ 0 };
    for (auto n : directorySubdirectoriesPaths)
    {
        i++;
        cout << i << ". " << n.string() << endl;
    }
}

vector<File> Directory::directoryGetFiles(void)
{
    return directoryFiles;
}

vector<fs::path> Directory::directoryGetSubdirectories(void)
{
    return directorySubdirectoriesPaths;
}

// only declaration, definition below
vector<string> extensionsFromString(string str);

void Directory::directoryFiltrExtensions(string extensions)
{
    vector<string> extensionsUsed = extensionsFromString(extensions);

    vector<File> filesToAnalyze;
    for (auto f : directoryFiles)
    {
        for (auto ext : extensionsUsed)
        {
            if (f.fileGetExt() == ext)
                filesToAnalyze.push_back(f);
        }
    }

    directoryFiles = filesToAnalyze;
}

void Directory::directoryGenerateReport(string reportName)
{
    int directoryLines{ 0 };
    int directoryChars{ 0 };
    int directoryWords{ 0 };
    int directoryFilesSize{ 0 };

    for (auto f : directoryFiles)
    {
        directoryLines += f.fileLines;
        directoryWords += f.fileWords;
        directoryChars += f.fileChars;
        directoryFilesSize += f.fileSize;
    }


    std::ofstream output_file("./" + reportName + ".txt");
    output_file << "---------- DIRECTORY INFORMATIONS ----------\n";
    output_file << "Direscory path: " << directoryPath.string() << endl;
    output_file << "Subdirestories [" << directorySubdirectoriesPaths.size() << "]:\n ";
    for (auto d : directorySubdirectoriesPaths)
    {
        output_file << d.string() << endl;
    }
    output_file << "\nDirectory and subdirectories contain " << directoryFiles.size() << " files with choosen extensions.\n";
    output_file << "These files are " << directoryFilesSize << " bytes size.\n";
    output_file << "These files contain:\n\t";
    output_file << "-" << directoryLines << " lines, \n\t";
    output_file << "-" << directoryWords << " words, \n\t";
    output_file << "-" << directoryChars << " chars. \n";

    output_file << "\n\n------------ FILES INFORMATIONS ------------" << endl;
    for (auto i : directoryFiles)
    {
        output_file << i << endl;
    }


    output_file.close();
}

/********************************************************
*****************   Other functions   *******************
*********************************************************/
/* 
    Check if input is directory path or file path.
    return - 2 if its path to file, 1 if its path to directory, 0 something else
*/
int checkPath(string userPath)
{
    if (fs::is_directory(userPath))
        return 2;
    else if (fs::is_regular_file(userPath))
        return 1;
    else
        return 0;  
}


/*
    Make vector of possible extensions from input string.
*/
vector<string> extensionsFromString(string str)
{
    vector<string> words;
    string word = "";

    // making a string stream 
    std::stringstream iss(str);
    while (iss >> word)
    {
        words.push_back(word);
    }

    if (words.size() < 1)
        return words;

    // delete non extension strings
    int i{ 0 };
    for (auto w : words)
    {
        if (w.size() < 2)
        {
            words.erase(words.begin() + i);
            i--;
        }
        else if (w[0] != '.')
        {
            words.erase(words.begin() + i);
            i--;
        }
        i++;
    }
    return words;
}