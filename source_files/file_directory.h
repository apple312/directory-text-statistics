#include <iostream>
#include <vector>
#include <string>
#include <filesystem>
#include <fstream>
#include <stdexcept>

using std::vector;
using std::string;
using std::cout;
using std::endl;

namespace fs = std::filesystem;

class File
{
private:
    fs::path filePath;
    string fileExtension{};
public:
    int fileSize{ 0 };
    int fileLines{ 0 };
    int fileChars{ 0 };
    int fileWords{ 0 };

    File(fs::path UserPath);
    fs::path fileGetPath(void);
    string fileGetExt(void);
};
std::ostream& operator<<(std::ostream& os, File& f);

class Directory
{
private:
    fs::path directoryPath;
    vector<File> directoryFiles;
    vector<fs::path> directorySubdirectoriesPaths;
public:
    Directory(fs::path UserPath);
    void directoryPrintFiles(void);
    void directoryPrintSubdirectories(void);
    vector<File> directoryGetFiles(void);
    vector<fs::path> directoryGetSubdirectories(void);
    void directoryFiltrExtensions(string extensions);
    void directoryGenerateReport(string raportName);
};

int checkPath(string userPath);
