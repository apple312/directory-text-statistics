# Directory Text Statistics 

Console app made in C++17 and Visual Studio 2019, allows user to check text statistics of files in directory and sub directories.
User can also specify what kind of files (extensions) should be taken for analyze.
It generates .txt report with details like:
*  all directory - number of text lines, number of words, number of chars
*  size of text files in directory
*  for every file - number of text lines, number of words, number of chars

[Technical report.](https://gitlab.com/apple312/directory-text-statistics/blob/master/technical_report_%C5%81ukasz_Hajec_ver_01.pdf) [POLISH]

*Project was made for Object Oriented Programming course at university.*  
